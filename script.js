function AiOZoom (classname, options) {

    this.all_options = {
        zoom: true,
        zoom_mobile:true,
        link_Atribute: 'href',
        error_Notice: 'The image could not be loaded',
        rtl: false,
        zoom_activation: 'hover',
        responsive: {},
        gallery: '',
        carouselGallery: false,
    }

    if (options) {
        if (options.hasOwnProperty('zoom')) this.all_options.zoom = options.zoom;
        if (options.hasOwnProperty('zoom_mobile')) this.all_options.zoom_mobile = options.zoom_mobile;
        if (options.hasOwnProperty('link_Atribute')) this.all_options.link_Atribute = options.link_Atribute;
        if (options.hasOwnProperty('error_Notice')) this.all_options.error_Notice = options.error_Notice;
        if (options.hasOwnProperty('rtl')) this.all_options.rtl = options.rtl;
        if (options.hasOwnProperty('zoom_activation')) this.zoom_activation = options.zoom_activation;
        if (options.hasOwnProperty('responsive')) this.responsive = options.responsive;
        if (options.hasOwnProperty('gallery')) this.gallery = options.gallery;
        if (options.hasOwnProperty('carouselGallery')) this.carouselGallery = options.carouselGallery;
    }
}

var Zoom = new AiOZoom('.classname', {some, options});